
#ifndef __HW_CONFIG_H
#define __HW_CONFIG_H

#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "em_ldma.h"

// DMA channel used for the example
#define LDMA_CHANNEL        0
#define LDMA_CH_MASK        (1 << LDMA_CHANNEL)

//PDM MIC PORTS and PINS
#define MIC_EN_PORT 			gpioPortB
#define MIC_EN_PIN  			2

#define MIC_PDM_PORT			gpioPortC
#define MIC_PDM_CLK_PORT 		gpioPortC
#define MIC_PDM_CLK_PIN			4
#define MIC_PDM_DATA0_PORT   	gpioPortC
#define MIC_PDM_DATA0_PIN		5

//MICRO SD PORTS and PINS
#define MICROSD_HI_SPI_FREQ     8000000
#define MICROSD_LO_SPI_FREQ     100000
#define MICROSD_USART           USART1 //USART0
#define MICROSD_CMUCLOCK        cmuClock_USART1 //cmuClock_USART2 //cmuClock_USART0
#define MICROSD_MISOPORT		gpioPortB	//gpioPortC = BRD4180A	//gpioPortC = BRD4183A
#define MICROSD_MISOPIN         0//8			//0 					//4 = BRD4183A
#define MICROSD_MOSIPORT		gpioPortB	//gpioPortC = BRD4180A 	//gpioPortC = BRD4183A
#define MICROSD_MOSIPIN         1//7			//1 					//5 = BRD4183A
#define MICROSD_CLKPORT			gpioPortA	//gpioPortD = BRD4180A  	//gpioPortA = BRD4183A
#define MICROSD_CLKPIN          6			//4 	 					//4 = BRD4183A
#define MICROSD_GPIOPORT        gpioPortA	//gpioPortC = BRD4180A 	//gpioPortA = BRD4183A
#define MICROSD_CSPIN           0			//2 					//0 = BRD4183A

//NOTE:: Change USART PORT # in microsd.c

#define TIME_MEASURE_GPIOPORT  	gpioPortA	//gpioPortB = BRD4180A 	//gpioPortB = BRD4183A
#define TIME_MEASURE_PIN		5			//0 					//1 = BRD4183A

#define TIME_MEASURE_GPIOPORT2  gpioPortA
#define TIME_MEASURE_PIN2		4

#endif // __HW_CONFIG_H

