/*
 * buffer_routines.h
 *
 *  Created on: Jul 8, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef BUFFER_ROUTINES_H_
#define BUFFER_ROUTINES_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>


#define BUF_SIZE 		(256*3)

// ECG byte count per sample
#define AUDIO_BYTE_COUNT  64

// Number of bytes per sub page
#define SUB_BUFFER_COUNT 256

#define SUB_LOCS 2

uint8_t bigBuffer[BUF_SIZE];

typedef struct {
	uint32_t currentAvailablePtr;
	uint32_t nextAvailablePtr;
	uint8_t pos;
	uint32_t *loc;
	bool isFull;
	bool readyToSend;

} indexes;

typedef struct{
	uint32_t subBufLocs[SUB_LOCS];
	uint32_t len;

} locationsToDescs;

indexes  get_indexes(void);
locationsToDescs get_subbuffer_memory_locs(void);

void fill_buffer(uint8_t smallBuffer[]);
//uint8_t * get_big_buffer(void);

#endif /* BUFFER_ROUTINES_H_ */
