/*
 * buffer_routines.c
 *
 *  Created on: Jul 8, 2020
 *      Author: Harsha Ganegoda
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "buffer_routines.h"
#include "hw_config.h"

uint32_t byteItteration = 0;
uint32_t byteCount = 0;
static indexes audioIdx;
locationsToDescs memLocs;

indexes get_indexes(void)
{
	return audioIdx;
}

locationsToDescs get_subbuffer_memory_locs(void)
{
	return memLocs;
}

/*
uint8_t * get_big_buffer(void)
{
	return bigBuffer;
}
*/

void fill_buffer(uint8_t smallBuffer[])
{
	audioIdx.currentAvailablePtr = audioIdx.nextAvailablePtr;
	memcpy(bigBuffer + audioIdx.currentAvailablePtr, smallBuffer, AUDIO_BYTE_COUNT);
	byteItteration++;
	byteCount = byteItteration * AUDIO_BYTE_COUNT;
	audioIdx.isFull = false;
	audioIdx.readyToSend = false;
	if(byteCount == SUB_BUFFER_COUNT)
	{
		audioIdx.isFull = true;
		byteCount = 0;
		byteItteration = 0;
		memLocs.subBufLocs[audioIdx.pos] = &bigBuffer[audioIdx.currentAvailablePtr - (SUB_BUFFER_COUNT - AUDIO_BYTE_COUNT)];
		audioIdx.pos++;
		if(audioIdx.pos == SUB_LOCS)
		{   byteItteration = 0;
			byteCount = 0;
			audioIdx.readyToSend = true;
			audioIdx.pos = 0;
		}
	}
	audioIdx.nextAvailablePtr = (audioIdx.nextAvailablePtr + AUDIO_BYTE_COUNT) % BUF_SIZE;
}



