/*
 * gpio_interrupt.c
 *
 *  Created on: Jul 8, 2020
 *      Author: Harsha Ganegoda
 */

#include "gpio_interrupt.h"
#include "hw_config.h"

#include "em_emu.h"
#include "em_gpio.h"
#include "em_int.h"
#include "em_cmu.h"

void init_gpio(void)
{
	// Enable GPIO clock
	CMU_ClockEnable(cmuClock_GPIO, true);

	// Configure RX pin as an input
	GPIO_PinModeSet(MICROSD_MISOPORT, MICROSD_MISOPIN, gpioModeInput, 0);
	// Configure TX pin as an output
	GPIO_PinModeSet(MICROSD_MOSIPORT, MICROSD_MOSIPIN, gpioModePushPull, 0);
	// Configure CLK pin as an output low (CPOL = 0)
	GPIO_PinModeSet(MICROSD_CLKPORT, MICROSD_CLKPIN, gpioModePushPull, 0);
	// Configure CS pin as an output and drive inactive high
	GPIO_PinModeSet(MICROSD_GPIOPORT, MICROSD_CSPIN, gpioModePushPull, 1);

	//Speed measure pin setup
	GPIO_PinModeSet(TIME_MEASURE_GPIOPORT, TIME_MEASURE_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(TIME_MEASURE_GPIOPORT2, TIME_MEASURE_PIN2, gpioModePushPull, 0);

}
