/*
 * microphone.c
 *
 *  Created on: Aug 31, 2020
 *      Author: Harsha Ganegoda
 */

#include <stdint.h>
#include "string.h"
#include <stdbool.h>
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_ldma.h"
#include "em_pdm.h"

#include "hw_config.h"
#include "microphone.h"
#include "microsd.h"
#include "buffer_routines.h"
#include "fatfs_file.h"

uint8_t *pdmBuffer;
uint8_t pdmBytes[PP_BUFFER_SIZE];


void initPdm(void)
{
	// Set up clocks
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_PDM, true);
	CMU_ClockSelectSet(cmuClock_PDM, cmuSelect_HFRCODPLL); // cmuSelect_HFRCODPLL 19 MHz

	// Config GPIO and pin routing
	GPIO_PinModeSet(MIC_EN_PORT, MIC_EN_PIN, gpioModePushPull, 1);    				// MIC_EN
	GPIO_PinModeSet(MIC_PDM_CLK_PORT, MIC_PDM_CLK_PIN, gpioModePushPull, 0);    	// PDM_CLK
	GPIO_PinModeSet(MIC_PDM_DATA0_PORT, MIC_PDM_DATA0_PIN, gpioModeInput, 0);       // PDM_DATA

	GPIO_SlewrateSet(MIC_PDM_PORT, 7, 7);

	GPIO->PDMROUTE.ROUTEEN = GPIO_PDM_ROUTEEN_CLKPEN;
	GPIO->PDMROUTE.CLKROUTE = (MIC_PDM_CLK_PORT << _GPIO_PDM_CLKROUTE_PORT_SHIFT)
                            						| (MIC_PDM_CLK_PIN << _GPIO_PDM_CLKROUTE_PIN_SHIFT);
	GPIO->PDMROUTE.DAT0ROUTE = (MIC_PDM_DATA0_PORT << _GPIO_PDM_DAT0ROUTE_PORT_SHIFT)
                        						   | (MIC_PDM_DATA0_PIN << _GPIO_PDM_DAT0ROUTE_PIN_SHIFT);

	while(PDM->SYNCBUSY != 0);
	PDM->CFG1 = (5 << _PDM_CFG1_PRESC_SHIFT); // Gives 3MHz clocks

	//Configure PDM
	PDM->CFG0 = (	_PDM_CFG0_NUMCH_ONE << _PDM_CFG0_NUMCH_SHIFT |
					_PDM_CFG0_CH0CLKPOL_NORMAL << _PDM_CFG0_CH0CLKPOL_SHIFT |				//1
					_PDM_CFG0_DATAFORMAT_RIGHT16 << _PDM_CFG0_DATAFORMAT_SHIFT |      		//3
					_PDM_CFG0_FORDER_FIFTH << _PDM_CFG0_FORDER_SHIFT);

	// Enable module
	PDM->EN = PDM_EN_EN;

	// Start filter
	while(PDM->SYNCBUSY != 0);
	PDM->CMD = PDM_CMD_START;

	// Configure DSR/ Gain
	while(PDM->SYNCBUSY != 0);
	PDM->CTRL = (( 6 << _PDM_CTRL_GAIN_SHIFT) | (64 << _PDM_CTRL_DSR_SHIFT)); // 0 64


/*
    // latest example code from github SiLabs/peripheral
	// Config PDM
	PDM->CFG0 = PDM_CFG0_STEREOMODECH01_DISABLE
				| PDM_CFG0_CH0CLKPOL_NORMAL
				| PDM_CFG0_FIFODVL_FOUR
				| PDM_CFG0_DATAFORMAT_DOUBLE16
				| PDM_CFG0_NUMCH_ONE
				| PDM_CFG0_FORDER_FIFTH;

	// This value changes PDM clock
	PDM->CFG1 = (5 << _PDM_CFG1_PRESC_SHIFT);

	// Enable module
	PDM->EN = PDM_EN_EN;

	// Start filter
	while(PDM->SYNCBUSY != 0);
	PDM->CMD = PDM_CMD_START;

	// Config DSR/Gain
	while(PDM->SYNCBUSY != 0);
	PDM->CTRL = (3 << _PDM_CTRL_GAIN_SHIFT) | (32 << _PDM_CTRL_DSR_SHIFT);

*/


}

void initLdma(void)
{
	LDMA_Init_t init = LDMA_INIT_DEFAULT;

	// LDMA transfers trigger on PDM Rx Data Valid
	LDMA_TransferCfg_t periTransferTx =
			LDMA_TRANSFER_CFG_PERIPHERAL(ldmaPeripheralSignal_PDM_RXDATAV);

	// Link descriptors for ping-pong transfer
	descLink[0] = (LDMA_Descriptor_t)
    								LDMA_DESCRIPTOR_LINKREL_P2M_WORD(&PDM->RXDATA, pingBuffer,
    										PP_BUFFER_SIZE, 1);
	descLink[1] = (LDMA_Descriptor_t)
    								LDMA_DESCRIPTOR_LINKREL_P2M_WORD(&PDM->RXDATA, pongBuffer,
    										PP_BUFFER_SIZE, -1);

	// Next transfer writes to pingBuffer
	prevBufferPing = false;

	LDMA_Init(&init);

	LDMA_StartTransfer(LDMA_CHANNEL, (void*)&periTransferTx, (void*)&descLink);
}

void LDMA_IRQHandler(void)
{
	uint32_t pending;

	// Read interrupt source
	pending = LDMA_IntGet();

	// Clear interrupts
	LDMA_IntClear(pending);

	// Check for LDMA error
	if(pending & LDMA_IF_ERROR) {
		// Loop here to enable the debugger to see what has happened
		while(1);
	}

	// Keep track of previously written buffer
	prevBufferPing = !prevBufferPing;
	// Read PDM data from pingpong buffer to pdmBuffer
	pdmBuffer = stream_pdm_samples(pdmBytes, prevBufferPing);
	// Buffer PDM data into BIG BUFFER
	fill_buffer(pdmBuffer);

}

uint8_t *stream_pdm_samples(uint8_t pdmBytes[], bool active_buffer)
{
	int i;
	if(active_buffer)
	{
		for(i=0; i<PP_BUFFER_SIZE; i++)
		{
			pdmBytes[i] = (pingBuffer[i] >> 8) & 0x000000FF;
			pdmBytes[i+1] = pingBuffer[i] & 0x000000FF;
		}
	}
	else
	{
		for(i=0; i<PP_BUFFER_SIZE; i++)
		{
			pdmBytes[i] = (pongBuffer[i] >> 8) & 0x000000FF;
			pdmBytes[i+1] = pongBuffer[i] & 0x000000FF;
		}
	}

	return pdmBytes;
}

void wavfile_open(void)
{
	int samples_per_second = SAMPLES_PER_SEC;
	int bits_per_sample = 16;
	strncpy(header.riff_tag,"RIFF",4);
	header.riff_length = 0;
	strncpy(header.wave_tag,"WAVE",4);
	strncpy(header.fmt_tag,"fmt ",4);
	header.fmt_length = 16; 				// 16 for PCM
	header.audio_format = 1; 				// PCM = 1
	header.num_channels = 1;
	header.sample_rate = samples_per_second;
	header.byte_rate = samples_per_second*(bits_per_sample/8);
	header.block_align = bits_per_sample/8;
	header.bits_per_sample = bits_per_sample;
	strncpy(header.data_tag,"data",4);
	header.data_length = 0;

	file_open_for_write();
	file_lseek(0);
	file_write(&header, sizeof(header));
	file_close();
}

void wavfile_close(void)
{
	int file_length = file_tell();
	int data_length = file_length - sizeof(struct wavfile_header);
	file_lseek(sizeof(struct wavfile_header) - sizeof(int));
	file_write(&data_length, sizeof(data_length));

	int riff_length = file_length - 8;
	file_lseek(4);
	file_write(&riff_length, sizeof(riff_length));

	file_close();
}


