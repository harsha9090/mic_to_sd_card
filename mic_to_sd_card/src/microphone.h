/*
 * microphone.h
 *
 *  Created on: Aug 31, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef SRC_MICROPHONE_H_
#define SRC_MICROPHONE_H_

#define SAMPLES_PER_SEC 22050 //44100 //44100

// Ping-pong buffer size
#define PP_BUFFER_SIZE      64
// Left/right buffer size
#define BUFFER_SIZE         128

// Buffers for left/right PCM data
//int16_t left[BUFFER_SIZE];
//int16_t right[BUFFER_SIZE];

// Descriptor linked list for LDMA transfer
LDMA_Descriptor_t descLink[2];

// Buffers for ping-pong transfer
uint32_t pingBuffer[PP_BUFFER_SIZE];
uint32_t pongBuffer[PP_BUFFER_SIZE];

// Keeps track of previously written buffer
bool prevBufferPing;

struct wavfile_header {
	char	riff_tag[4];
	int		riff_length;
	char	wave_tag[4];
	char	fmt_tag[4];
	int		fmt_length;
	short	audio_format;
	short	num_channels;
	int		sample_rate;
	int		byte_rate;
	short	block_align;
	short	bits_per_sample;
	char	data_tag[4];
	int		data_length;
};

struct wavfile_header header;

void initPdm(void);
void initLdma(void);
void LDMA_IRQHandler(void);
uint8_t *stream_pdm_samples(uint8_t pdmBytes[], bool active_buffer);
void wavfile_open(void);
void wavfile_close(void);

#endif /* SRC_MICROPHONE_H_ */
