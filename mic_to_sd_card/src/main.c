/*
 * This code is checked on SiLabs BRD4183A radio board
 * which has EFR32MG22 Series 2 chip
 *
 * Buffering audio data into micro sd card as in .wav file
 *
 */

#include "em_device.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_dbg.h"
#include "em_timer.h"
#include "em_chip.h"
#include "stdio.h"
#include "fatfs_file.h"
#include "hw_config.h"
#include "events.h"
#include "gpio_interrupt.h"
#include "microphone.h"
#include "buffer_routines.h"

static indexes buffer_idxs;
locationsToDescs mem_idxs;
uint32_t first_write_loc = 0;

int main(void)
{
	// Chip errata
	CHIP_Init();
	// Initialization of GPIO and IRQ
	init_gpio();
	// Initialization of micro sd card
	init_micro_sd();
	file_mount();
	// Initialization of LDMA and PDM
	initLdma();
	initPdm();
	// Open file with WAV header
	wavfile_open();
	first_write_loc = sizeof(header);

	while(1)
	{
		buffer_idxs = get_indexes();
		if(buffer_idxs.readyToSend)
		{
			mem_idxs = get_subbuffer_memory_locs();
			file_open_for_write();
			for(uint8_t i = 0; i < SUB_LOCS; i++)
			{
				file_lseek(first_write_loc);
				uint8_t *b = (uint8_t *)(mem_idxs.subBufLocs[i]);
				pdm_write_to_file(b, SUB_BUFFER_COUNT);
				first_write_loc += SUB_BUFFER_COUNT;
			}
			// Wave file is closed with updated file length
			wavfile_close();
		}
	}
}
