/*
 * microsdconfig.h
 *
 *  Created on: Aug 19, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef __MICROSDCONFIG_H
#define __MICROSDCONFIG_H

/*
// Don't increase MICROSD_HI_SPI_FREQ beyond 8MHz. Next step will be 16MHz
// which is out of spec
#define MICROSD_HI_SPI_FREQ     8000000 //8000000

#define MICROSD_LO_SPI_FREQ     100000
#define MICROSD_USART           USART1 //USART0
#define MICROSD_CMUCLOCK        cmuClock_USART1 //cmuClock_USART2 //cmuClock_USART0


#define MICROSD_MISOPORT		gpioPortB	//gpioPortC = BRD4180A	//gpioPortC = BRD4183A
#define MICROSD_MISOPIN         0//8			//0 					//4 = BRD4183A
#define MICROSD_MOSIPORT		gpioPortB	//gpioPortC = BRD4180A 	//gpioPortC = BRD4183A
#define MICROSD_MOSIPIN         1//7			//1 					//5 = BRD4183A
#define MICROSD_CLKPORT			gpioPortA	//gpioPortD = BRD4180A  	//gpioPortA = BRD4183A
#define MICROSD_CLKPIN          6			//4 	 					//4 = BRD4183A

#define MICROSD_GPIOPORT        gpioPortA	//gpioPortC = BRD4180A 	//gpioPortA = BRD4183A
#define MICROSD_CSPIN           0			//2 					//0 = BRD4183A

//NOTE:: Change USART PORT # in microsd.c

#define TIME_MEASURE_GPIOPORT  	gpioPortA	//gpioPortB = BRD4180A 	//gpioPortB = BRD4183A
#define TIME_MEASURE_PIN		5			//0 					//1 = BRD4183A

#define TIME_MEASURE_GPIOPORT2  gpioPortA
#define TIME_MEASURE_PIN2		4
*/

#endif /* MICROSDCONFIG_H */
