/*
 * fatfs_file.h
 *
 *  Created on: Aug 25, 2020
 *      Author: Harsha Ganegoda
 */

#ifndef SRC_FATFS_FILE_H_
#define SRC_FATFS_FILE_H_


void init_micro_sd(void);
void file_mount(void);
void file_open_for_write();
void file_lseek(uint32_t start_location);
void pdm_write_to_file(uint8_t buffer_to_write[], uint32_t num_bytes);
void file_write(uint8_t buffer_to_write[], uint32_t num_bytes);
void file_close(void);
void file_open_for_read(void);
void file_read(void);
int file_tell(void);


#endif /* SRC_FATFS_FILE_H_ */
